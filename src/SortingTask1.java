import java.util.Random;

public class SortingTask1 {
    public static void main(String[] args) {
        char[] array = new char[15];
        initialize(array);
        display("Unsorted array:", array);
        selectionSort(array);
        display("Selection sorting:", array);
        initialize(array);
        display("Unsorted array:", array);
        bubbleSort(array);
        display("Bubble sorting:", array);
        initialize(array);
        display("Unsorted array:", array);
        insertionSort(array);
        display("Insertion sorting:", array);

    }
    public static void selectionSort(char[] arr) {
        int pos;
        char temp;
        for (int i = 0; i < arr.length; i++) {
            pos = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[pos]) {
                    pos = j;
                }
            }

            temp = arr[pos];
            arr[pos] = arr[i];
            arr[i] = temp;
        }
    }

    public static void bubbleSort(char[] arr) {

        boolean isSorted = false;
        char buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < arr.length-1; i++) {
                if(arr[i] > arr[i+1]){
                    isSorted = false;

                    buf = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = buf;
                }
            }
        }
    }

    public static void insertionSort(char[] arr) {
        for (int i = 1; i < arr.length; i++) {
            char currElem = arr[i];
            int prevKey = i - 1;
            while (prevKey >= 0 && arr[prevKey] > currElem) {
                arr[prevKey + 1] = arr[prevKey];
                arr[prevKey] = currElem;
                prevKey--;
            }
        }
    }
    public static void initialize(char[] arr) {
        Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (char)rand.nextInt(1000);
        }
    }

    public static void display(String message, char[] arr){
        System.out.println(message);
        for (int element : arr) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
}
